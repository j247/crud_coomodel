
SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8 ;
USE `mydb` ;

-- -----------------------------------------------------
-- Table `mydb`.`FELIGRESES`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`FELIGRESES` (
  `CEDULA` INT NOT NULL,
  `NOMBRE` VARCHAR(45) NOT NULL,
  `CIUDAD` VARCHAR(255) NOT NULL,
  `EDAD` INT NOT NULL,
  PRIMARY KEY (`CEDULA`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`MENORES`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`MENORES` (
  `ID` INT NOT NULL AUTO_INCREMENT,
  `NOMBRE` VARCHAR(45) NULL,
  `EDAD` INT NULL,
  `FELIGRESES_CEDULA` INT NOT NULL,
  PRIMARY KEY (`ID`),
  INDEX `fk_MENORES_FELIGRESES_idx` (`FELIGRESES_CEDULA` ASC) VISIBLE,
  CONSTRAINT `fk_MENORES_FELIGRESES`
    FOREIGN KEY (`FELIGRESES_CEDULA`)
    REFERENCES `mydb`.`FELIGRESES` (`CEDULA`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`EVENTO`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`EVENTO` (
  `ID` INT NOT NULL AUTO_INCREMENT,
  `NOMBRE` VARCHAR(45) NOT NULL,
  `FECHA` DATE NOT NULL,
  `HORA` TIME NOT NULL,
  `CAPACIDAD` INT NULL,
  PRIMARY KEY (`ID`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`INGRESO`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`INGRESO` (
  `ID` INT NOT NULL AUTO_INCREMENT,
  `FECHA` DATE NOT NULL,
  `HORA` TIME NOT NULL,
  `EVENTO_ID` INT NOT NULL,
  `FELIGRESES_CEDULA` INT NOT NULL,
  PRIMARY KEY (`ID`),
  INDEX `fk_INGRESO_EVENTO1_idx` (`EVENTO_ID` ASC) VISIBLE,
  INDEX `fk_INGRESO_FELIGRESES1_idx` (`FELIGRESES_CEDULA` ASC) VISIBLE,
  CONSTRAINT `fk_INGRESO_EVENTO1`
    FOREIGN KEY (`EVENTO_ID`)
    REFERENCES `mydb`.`EVENTO` (`ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_INGRESO_FELIGRESES1`
    FOREIGN KEY (`FELIGRESES_CEDULA`)
    REFERENCES `mydb`.`FELIGRESES` (`CEDULA`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
