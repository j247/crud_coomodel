package coomodel;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import conn.Conn;

public class FeligresesCoomodel extends Conn
{	
	static String cod;
	static String name; 
	static String city; 
	static String age;
	public static String create(String data[])
	{	
			cod = data[0];
			name = data[1];
			city = data[2];
			age = data[3];		
	    try 
	    {	    	
	    	con();	    	
	    	command.executeUpdate("CALL insert_feligreses("+ cod +",'"+name+"','"+city+"',"+ age+")");
	    	//command.executeUpdate("INSERT INTO feligreses(cedula, nombre, ciudad, edad) VALUES ("+ cod +",'"+name+"','"+city+"',"+ age+")");
			System.out.println("Almacenado");
			conexion.close();
		} catch(SQLException ex){
			System.out.println("Code Create");
			System.out.print(ex.toString());
		}
		return "Ok";	    
	}
	
	public static String[] read(int id)
	{
		String resul[] = new String[4];
		ResultSet readAll = null;
		try 
	    {	    	
	    	con();
	    	readAll= command.executeQuery("SELECT * FROM feligreses WHERE cedula="+id);
	    	
	        while(readAll.next()) 
	        {
	        	resul[0] =(readAll.getString("cedula"));
				resul[1] =(readAll.getString("nombre"));
				resul[2] =(readAll.getString("ciudad"));
				resul[3] =(readAll.getString("edad"));
				
	        }   
	    conexion.close();
		} catch(SQLException ex){
			System.out.println("Code");
			System.out.print(ex.toString());
		}
		return resul;			    
	}	
	
	public static String update(String data[])
	{
		String ok = "No";
		cod = data[0];
		name = data[1];
		city = data[2];
		age = data[3];
	    try 
	    {	    	
	    	con();	    	
	    	int valueUp= command.executeUpdate("UPDATE feligreses SET  nombre = "
	    			+ "'"+name +"', ciudad = '"+city +"', edad ="+age +" WHERE cedula ="+cod);
			if (valueUp == 1) 
			{
				System.out.println("Actualizado");
				ok="Si";
			} 
			else {
				System.out.println("Sin resultados para: update");
				ok="No";
			}
			conexion.close();
		} catch(SQLException ex){
			System.out.println("Code Update: ");
			System.out.print(ex.toString());
		}
		return ok;
	}
	
	public static String delete(int id)
	{
		String ok="No";
	    try 
	    {	    	
	    	con();
	       int valueDel= command.executeUpdate("DELETE FROM feligreses WHERE cedula ="+id);
			if (valueDel == 1) 
			{
				System.out.println("Eliminado");
				 ok = "Si";
			} 
			else {
				System.out.println("Sin resultados para: delete");
				ok = "No";
			}
		conexion.close();
		} catch(SQLException ex){
			System.out.println("Code Delete: ");
			System.out.print(ex.toString());
		}
		return ok;
		    
	}
			
	/* main test
	 * public static void main(String[] args) 
	{
		//consultAll();
		//delete();
		//create();
		//update();
	}*/
}
