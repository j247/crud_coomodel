package coomodel;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import conn.Conn;

public class IngresoCoomodel extends Conn
{
	static String fecha, hora;
	static String evento_id, feligreses_cedula, eventoId;
	static ResultSet resultado;
	
	//run --> comboBox
	public static ArrayList<String> comboEvento( )
	{	
		ArrayList<String> lista = new ArrayList<String>();	
	
		try 
		{
			con();	    	
			resultado = command.executeQuery("SELECT id, nombre FROM evento");
			System.out.println("Ok");
			while (resultado.next()) 
			{
				lista.add(resultado.getString("NOMBRE") +"_"+ resultado.getString("ID"));				
			}
			conexion.close();
		} catch(SQLException ex){
			System.out.println("error en comboEvento");
			System.out.print(ex.toString());
		}		
		return lista;	    
	}
	
	//MODELS CRUD
	public static String create(String data[])
	{	
			fecha = data[0];
			hora = data[1];
			evento_id = data[2];
			String[] separator_id = evento_id.split("_");
			eventoId = separator_id[1];
			//feligreses_cedula = data[3];
			feligreses_cedula = "33"; // temporal, para ejecutar el metodo Create
			
			System.out.println("Fecha: " + fecha);
			System.out.println("hora: " + hora);
			System.out.println("evento: " + eventoId);
	    try 
	    {	    	
	    	con();	    	
	    	command.executeUpdate("INSERT INTO ingreso(fecha, hora, evento_id, feligreses_cedula) VALUES ('"+ fecha +"','"+hora+"','"+eventoId+"',"+ feligreses_cedula+")");
			System.out.println("Almacenado");
			conexion.close();
		} catch(SQLException ex){
			System.out.println("error en create ingreso");
			System.out.print(ex.toString());
		}
		return "Ok";	    
	}
	
}
