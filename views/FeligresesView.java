package views;

import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import coomodel.FeligresesCoomodel;

import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import java.awt.Color;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextArea;


public class FeligresesView extends  JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField textField_cedula;
	private JTextField textField_nombre;
	private JTextField textField_ciudad;
	private JTextField textField_edad;
	FeligresesCoomodel mtd ;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FeligresesView frame = new FeligresesView();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FeligresesView() {
		setResizable(false);
		setBackground(Color.WHITE);
		setTitle("CRUD Feligreses");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 443, 402);
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.setBackground(Color.WHITE);
		menuBar.setForeground(Color.BLACK);
		setJMenuBar(menuBar);
		
		JMenu JMenu_opciones = new JMenu("Opciones");
		menuBar.add(JMenu_opciones);
		
		JMenuItem itemExit = new JMenuItem("Salir");
		itemExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				System.exit(0);
			}
		});
		
		JMenuItem itemClean = new JMenuItem("Limpiar");
		itemClean.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				 textField_cedula.setText("");
				 textField_nombre.setText("");
				 textField_ciudad.setText("");
				 textField_edad.setText("");
			}
		});
		JMenu_opciones.add(itemClean);
		JMenu_opciones.add(itemExit);
		
		JMenuItem itemIngreso = new JMenuItem("Ingresos");
		itemIngreso.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Ingreso dialog = new Ingreso();
                dialog.setVisible(true);
			}
		});
		JMenu_opciones.add(itemIngreso);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		textField_cedula = new JTextField();
		textField_cedula.setBounds(38, 89, 151, 20);
		contentPane.add(textField_cedula);
		textField_cedula.setColumns(10);
		
		textField_nombre = new JTextField();
		textField_nombre.setBounds(224, 89, 151, 20);
		contentPane.add(textField_nombre);
		textField_nombre.setColumns(10);
		
		textField_ciudad = new JTextField();
		textField_ciudad.setBounds(36, 168, 151, 20);
		contentPane.add(textField_ciudad);
		textField_ciudad.setColumns(10);
		
		textField_edad = new JTextField();
		textField_edad.setBounds(212, 169, 151, 20);
		contentPane.add(textField_edad);
		textField_edad.setColumns(10);
		
		final JTextArea textArea_all = new JTextArea();
				
		JButton btn_consultar = new JButton("Consultar");
		btn_consultar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int id = Integer.parseInt(textField_cedula.getText());
				 String[] res = FeligresesCoomodel.read(id);
				 
					 textField_nombre.setText(res[1]);
					 textField_ciudad.setText(res[2]);
					 textField_edad.setText(res[3]);				 	
					textArea_all.setText("Consulta OK");								
			}
		});
		btn_consultar.setBounds(28, 19, 89, 23);
		contentPane.add(btn_consultar);
		
		JButton btn_eliminar = new JButton("Eliminar");
		btn_eliminar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int id = Integer.parseInt(textField_cedula.getText());
				 String res = FeligresesCoomodel.delete(id);
				 textArea_all.setText("¿Eliminado? \n"+ res);
				 textField_cedula.setText("");
				 textField_nombre.setText("");
				 textField_ciudad.setText("");
				 textField_edad.setText("");
			}
		});
		btn_eliminar.setBounds(127, 19, 89, 23);
		contentPane.add(btn_eliminar);
		
		JButton btn_actualizar = new JButton("Actualizar");
		btn_actualizar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String cedula = textField_cedula.getText();
				String nombre = textField_nombre.getText();
				String ciudad = textField_ciudad.getText();
				String edad = textField_edad.getText();
				String data[] = {cedula, nombre, ciudad, edad };
				String res = FeligresesCoomodel.update(data);
				textArea_all.setText("Actualizados\n"+ res);
				textField_cedula.setText("");
				textField_nombre.setText("");
				textField_ciudad.setText("");
				textField_edad.setText("");
			}
		});
		btn_actualizar.setBounds(224, 19, 101, 23);
		contentPane.add(btn_actualizar);
		
		JButton btn_crear = new JButton("Crear");
		btn_crear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String cedula = textField_cedula.getText();
				String nombre = textField_nombre.getText();
				String ciudad = textField_ciudad.getText();
				String edad = textField_edad.getText();
				String data[] = {cedula, nombre, ciudad, edad };
				String res = FeligresesCoomodel.create(data);
				textArea_all.setText("Todos en Lista Almacenados\n"+ res);
				textField_cedula.setText("");
				textField_nombre.setText("");
				textField_ciudad.setText("");
				textField_edad.setText("");
			}
		});
		btn_crear.setBounds(335, 19, 66, 23);
		contentPane.add(btn_crear);
		
		JLabel lblNewLabel_1 = new JLabel("Identificaci\u00F3n");
		lblNewLabel_1.setBounds(38, 107, 66, 14);
		contentPane.add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("Nombre");
		lblNewLabel_2.setBounds(224, 108, 46, 14);
		contentPane.add(lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("Ciudad");
		lblNewLabel_3.setBounds(36, 187, 46, 14);
		contentPane.add(lblNewLabel_3);
		
		JLabel lblNewLabel_4 = new JLabel("Edad");
		lblNewLabel_4.setBounds(212, 187, 46, 14);
		contentPane.add(lblNewLabel_4);
		
		textArea_all.setBounds(28, 226, 335, 84);
		contentPane.add(textArea_all);
		
		JLabel lbl_autor = new JLabel("Autor: Huber Arboleda (septiembre 2021)");
		lbl_autor.setForeground(Color.RED);
		lbl_autor.setBounds(59, 326, 234, 14);
		contentPane.add(lbl_autor);
		
	}
}
