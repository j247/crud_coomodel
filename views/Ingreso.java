package views;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import coomodel.IngresoCoomodel;

public class Ingreso extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField tf_fecha;
	private JTextField tf_hora;
	
	static ArrayList<String> lista = new ArrayList<String>();
	LocalDate date = LocalDate.now(); 
	LocalTime hour = LocalTime.now(); 
	int hours   =  hour.getHour();
	int minutes = hour.getMinute();
	int seconds = hour.getSecond();
	String hora = hours  + ":"+ minutes +":"+seconds;
	
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			Ingreso dialog = new Ingreso();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public Ingreso() {
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(null);
		contentPanel.setBounds(0, 0, 434, 228);
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel);
		contentPanel.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Registrar Ingreso");
		lblNewLabel.setBounds(142, 11, 142, 14);
		contentPanel.add(lblNewLabel);
		
		tf_fecha = new JTextField();
		tf_fecha.setBounds(215, 36, 86, 20);
		contentPanel.add(tf_fecha);
		tf_fecha.setColumns(10);
		tf_fecha.setText(""+date);
		
		tf_hora = new JTextField();
		tf_hora.setBounds(215, 67, 86, 20);
		contentPanel.add(tf_hora);
		tf_hora.setColumns(10);
		tf_hora.setText(""+hora);
		
		
		//CB Evento
		final JComboBox cb_evento = new JComboBox();
		lista = IngresoCoomodel.comboEvento();
		for (int i = 0; i < lista.size(); i++) 
		{
			cb_evento.addItem(lista.get(i)); 
		}
		cb_evento.setBounds(215, 110, 200, 22);
		contentPanel.add(cb_evento);
		
		//CB Feligreses
		final JComboBox cb_feligreses = new JComboBox();
		cb_feligreses.setBounds(215, 153, 30, 22);
		contentPanel.add(cb_feligreses);
		
		JLabel lblNewLabel_1 = new JLabel("Fecha");
		lblNewLabel_1.setBounds(67, 39, 46, 14);
		contentPanel.add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("Hora");
		lblNewLabel_2.setBounds(67, 70, 46, 14);
		contentPanel.add(lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("Evento");
		lblNewLabel_3.setBounds(67, 114, 46, 14);
		contentPanel.add(lblNewLabel_3);
		
		JLabel lblNewLabel_4 = new JLabel("Feligres");
		lblNewLabel_4.setBounds(67, 157, 46, 14);
		contentPanel.add(lblNewLabel_4);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setBounds(0, 228, 434, 33);
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						String fecha = tf_fecha.getText();
						String hora = tf_hora.getText();
						String evento = (String) cb_evento.getSelectedItem();
						String feligreses = (String) cb_feligreses.getSelectedItem();
						String data[] = {fecha, hora, evento, feligreses};
						String res = IngresoCoomodel.create(data);												
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
		}
	}
}
