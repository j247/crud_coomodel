package conn;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class Conn 
{
	protected static Connection conexion;
	protected static Statement command;
	static String pass = "";
	static String user = "root";
	static String nameDB = "mydb";
	static String url 	= "jdbc:mysql://127.0.0.1:3306/"+nameDB+"?user="+user+"&password="+pass;
		
	public static Connection con() throws SQLException
	{
		conexion=DriverManager.getConnection(url);
		command=conexion.createStatement();
		return conexion;
	}
}
